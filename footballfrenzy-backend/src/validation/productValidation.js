import Joi from "joi";

let productValidation = Joi.object()
  .keys({
    title: Joi.string().min(5).max(30).required().messages({
      "any.required": "title is required",
    }),
    description: Joi.array().items(Joi.string()).min(1).required().messages({
      "any.required": "description is required",
    }),
    categories: Joi.array().items(Joi.string()).min(1).required(),
    imageUrls: Joi.array().items(Joi.string()).min(1).required(),
    actualPrice: Joi.number().required().min(1).max(100000),
    discount: Joi.number().required().min(0).max(100),
    rating: Joi.number(),
    sellingPrice: Joi.number().custom((value, helpers) => {
      const actualPrice = helpers.state.ancestors[0].actualPrice;
      const discount = helpers.state.ancestors[0].discount;
      const sellingPrice = actualPrice - (actualPrice * discount) / 100;
      return sellingPrice;
    }),
  })
  .unknown(false);
export default productValidation;
