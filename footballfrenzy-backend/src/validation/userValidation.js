import Joi from "joi";

let userValidation = Joi.object().keys({
  fullName: Joi.string().min(3).max(25).required().messages({
    "any.required": "fullName is required",
  }),
  email: Joi.string()
    .required()
    .custom((value, msg) => {
      let validEmail = value.match(
        /^[\w-]+(\.[\w-]+)*@([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,}$/
      );
      if (validEmail) {
        return true;
      } else {
        return msg.message("Invalid email address.");
      }
    }),
  password: Joi.string()
    .min(8)
    .required()
    .pattern(
      /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*])[A-Za-z0-9!@#$%^&*]{8,16}$/
    )
    .messages({
      "any.required": "Password is required",
      "string.pattern":
        "Password must include uppercase, lowercase letters, numbers and symbols",
    }),
  phoneNumber: Joi.string().required().messages({
    "any.required": "phoneNumber is required",
  }),
  address: Joi.string().required().messages({
    "any.required": "address is required",
  }),
  gender: Joi.string().required().valid("male", "female", "others"),
  role: Joi.string().required().valid("customer", "admin", "superadmin"),
  isEmailVerfied: Joi.boolean().default(false),
  wishlist: Joi.array().items(Joi.string()),
  orders: Joi.array().items(Joi.string()),
});
