import jwt from "jsonwebtoken";
import { secretKey } from "../constant.js";

let isAuthenticated = async (req, res, next) => {
  try {
    let tokenString = req.headers.authorization;
    // console.log(tokenString.split(" ")[1]); //the postman adds "bearer" before the token so we need to split and get the token only
    let token = tokenString.split(" ")[1];
    let user = await jwt.verify(token, secretKey);
    req._id = user._id; // this line sends the id to another middleware
    next();
  } catch (error) {
    res.status(401).json({
      success: false,
      message: "Invalid Token",
    });
  }
};
export default isAuthenticated;
