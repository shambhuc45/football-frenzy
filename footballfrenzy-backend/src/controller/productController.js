import {
  createProductService,
  deleteProductService,
  readAllProductService,
  readSpecificProductService,
  updateProductService,
} from "../services/productService.js";

export let createProductController = async (req, res, next) => {
  let data = req.body;
  try {
    let result = await createProductService(data);
    res.status(201).json({
      status: "true",
      message: "Successfully Created",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      status: false,
      message: error.message,
    });
  }
};

export let readAllProductsController = async (req, res, next) => {
  let result = await readAllProductService();
  res.status(200).json({
    status: true,
    result: result,
  });
};

export let readSpecificProductController = async (req, res, next) => {
  try {
    let id = req.params.id;
    let result = await readSpecificProductService(id);
    res.status(200).json({
      status: true,
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      status: false,
      message: error.message,
    });
  }
};
export let updateSpecificProductController = async (req, res, next) => {
  try {
    let result = await updateProductService(req.params.id, req.body);
    res.status(201).json({
      status: true,
      message: "Product Updated Successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      status: false,
      message: error.message,
    });
  }
};
export let deleteProductController = async (req, res, next) => {
  try {
    let result = await deleteProductService(req.params.id);
    res.status(200).json({
      status: true,
      message: "Product Deleted Successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      status: false,
      message: error.message,
    });
  }
};
