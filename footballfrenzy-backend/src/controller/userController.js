import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { clientUrl, secretKey } from "../constant.js";
import { sendEmail } from "../utils/sendMail.js";
import { User } from "../model/model.js";

export let registerUserController = async (req, res, next) => {
  let data = req.body;
  data.password = await bcrypt.hash(data.password, 10);

  try {
    let result = await User.create(data);

    let infoObj = {
      _id: result._id,
    };
    let expiryInfo = {
      expiresIn: "7d",
    };
    let token = jwt.sign(infoObj, secretKey, expiryInfo);

    await sendEmail({
      to: [req.body.email],
      subject: "Account Registration",
      html: `
      <p>Thank you for Registering. Please verify your email through provided link below.</p><br/>
      <a href="${clientUrl}/verify-email?token=${token}">
      ${clientUrl}/verify-email?token=${token}
      </a>
      `,
    });
    res.status(201).json({
      success: true,
      message: "User created successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let verifyEmail = async (req, res, next) => {
  try {
    let tokenString = req.headers.authorization;
    // console.log(tokenString.split(" ")[1]); //the postman adds "bearer" before the token so we need to split and get the token only
    let token = tokenString.split(" ")[1];

    let infoObj = await jwt.verify(token, secretKey);
    let userId = infoObj._id;

    let result = await User.findByIdAndUpdate(
      userId,
      {
        isEmailVerified: true,
      },
      {
        new: true,
      }
    );
    res.status(201).json({
      success: true,
      message: "User Verified successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let loginUserController = async (req, res, next) => {
  try {
    // take user input value
    let email = req.body.email;
    let password = req.body.password;
    //if the email exist take that object from the db
    let user = await User.findOne({ email: email });
    if (user) {
      // check isEmailVerified
      if (user.isEmailVerified === true) {
        // check password
        let dbPassword = user.password;
        let isValidPassword = await bcrypt.compare(password, dbPassword);
        if (isValidPassword === true) {
          let infoObj = {
            _id: user._id,
          };
          let expiryInfo = {
            expiresIn: "7 days",
          };
          let token = await jwt.sign(infoObj, secretKey, expiryInfo);
          res.status(200).json({
            success: true,
            message: "Logged in successfully",
            result: user,
            token: token,
          });
        } else {
          let error = new Error("Email or Password did not match.");
          throw error;
        }
      } else {
        let error = new Error("Email is not verified.");
        throw error;
      }
    } else {
      let error = new Error("Credentials doesn't match with our Database.");
      throw error;
    }
  } catch (error) {
    res.status(401).json({
      success: false,
      message: error.message,
    });
  }
};
export let updatePassword = async (req, res, next) => {
  try {
    let userId = req._id;
    let oldPassword = req.body.oldPassword;
    let newPassword = req.body.newPassword;
    let data = await User.findById(userId);
    let dbPassword = data.password;
    let isValidPassword = await bcrypt.compare(oldPassword, dbPassword);
    if (isValidPassword) {
      let newHashPassword = await bcrypt.hash(newPassword, 10);

      let result = await User.findByIdAndUpdate(
        userId,
        {
          password: newHashPassword,
        },
        {
          new: true,
        }
      );
      res.status(201).json({
        success: true,
        message: "Password Updated successfully",
        result: result,
      });
    } else {
      let error = new Error("Incorrect Password");
      throw error;
    }
  } catch (error) {
    res.status(400).json({
      success: false,
      error: error.message,
    });
  }
};
export let forgotPassword = async (req, res, next) => {
  try {
    let email = req.body.email;
    let result = await User.findOne({ email: email });
    if (result !== null) {
      let infoObj = {
        _id: result._id,
      };
      let expiryInfo = {
        expiresIn: "10m",
      };
      let token = await jwt.sign(infoObj, secretKey, expiryInfo);
      await sendEmail({
        to: email,
        subject: "Forgot Password",
        html: `
          <h4>Please verify that its you trying to reset your password.</h4><br/>
          <a href="${clientUrl}/reset-password?token=${token}">
          ${clientUrl}/reset-password?token=${token}
          </a><br/>
          <h4>If you did not do this don't do anything.</h4><br/>
          `,
      });
      res.status(200).json({
        success: true,
        message: "Password reset link has been sent to your email.",
      });
    } else {
      let error = new Error("Email not found.");
      error.status = 404;
      throw error;
    }
  } catch (error) {
    res.status(error.status || 400).json({
      success: false,
      message: error.message,
    });
  }
};
export let resetPassword = async (req, res, next) => {
  try {
    let hashPassword = await bcrypt.hash(req.body.password, 10);
    let result = await User.findByIdAndUpdate(
      req._id,
      {
        password: hashPassword,
      },
      {
        new: true,
      }
    );
    res.status(201).json({
      success: true,
      message: "Password reset successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let myProfile = async (req, res, next) => {
  try {
    let userId = req._id;
    let result = await User.findById(userId);
    res.status(200).json({
      success: true,
      result: result,
    });
  } catch (error) {
    json.status(400).json({
      success: false,
      error: error.message,
    });
  }
};
export let updateProfile = async (req, res, next) => {
  try {
    let userId = req._id;
    let data = req.body;
    delete data.email;
    delete data.password;

    let result = await User.findByIdAndUpdate(userId, data, {
      new: true,
    });
    res.status(201).json({
      success: true,
      message: "Profile Updated successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      error: error.message,
    });
  }
};
export let readAllUserController = async (req, res, next) => {
  try {
    let result = await User.find({});
    res.status(200).json({
      success: true,
      message: "All User Read Successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      error: error.message,
    });
  }
};
export let readSpecificUserController = async (req, res, next) => {
  try {
    let result = await User.findById(req.params.id);
    res.status(200).json({
      success: true,
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let updateSpecificUserController = async (req, res, next) => {
  try {
    let data = req.body;
    delete data.email;
    delete data.password;
    let result = await User.findByIdAndUpdate(req.params.id, data, {
      new: true,
    });
    res.status(201).json({
      success: true,
      message: "User Updated successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
export let deleteUserController = async (req, res, next) => {
  try {
    let result = await User.findByIdAndDelete(req.params.id);
    res.status(200).json({
      success: true,
      message: "User Deleted successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
