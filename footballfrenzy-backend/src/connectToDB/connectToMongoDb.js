import mongoose from "mongoose";
import { dbUrl } from "../constant.js";

const connectToMongoDb = () => {
  mongoose.connect(dbUrl);
  console.log("Successfully, connected to MongoDB");
};
export default connectToMongoDb;
