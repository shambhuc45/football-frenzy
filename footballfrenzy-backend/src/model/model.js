import { model } from "mongoose";
import productSchema from "../schema/productSchema.js";
import userSchema from "../schema/userSchema.js";

export let User = model("User", userSchema);
export let Product = model("Product", productSchema);
