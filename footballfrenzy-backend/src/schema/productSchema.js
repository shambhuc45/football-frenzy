import { Schema } from "mongoose";

let productSchema = Schema(
  {
    title: {
      type: String,
      required: true,
      trim: true,
    },
    description: {
      type: String,
      trim: true,
    },
    featured: {
      type: Boolean,
      default:false
    },
    category: [{ type: String, required: true }],
    imageUrls: { type: String, trim: true },
    actualPrice: {
      type: Number,
    },
    stock: {
      type: Number,
      default: 0,
      min: 0,
    },
    discount: Number,
    sellingPrice: {
      type: Number,
    },
    rating: Number,
  },
  { timestamps: true }
);
export default productSchema;
