import { Schema } from "mongoose";

//Defining Schema.

let userSchema = Schema(
  {
    fullName: {
      type: String,
      trim: true,
      required: [true, "fullName is required"],
    },
    email: {
      unique: true,
      type: String,
      trim: true,
      required: [true, "email is required"],
      lowercase: true,
    },
    password: {
      type: String,
      trim: true,
      required: [true, "password is required"],
    },
    phoneNumber: {
      type: String,
      trim: true,
      required: [true, "phoneNumber is required"],
    },
    address: {
      type: String,
      trim: true,
      required: [true, "address is required"],
    },
    gender: {
      type: String,
      trim: true,
      default: "male",
    },
    role: {
      type: String,
      trim: true,
      enum: ["customer", "admin", "superadmin"],
      required: [true, "role is required"],
    },
    isEmailVerified: {
      type: Boolean,
      trim: true,
      default: false,
    },
    wishlist: [{ type: String }],
    orders: [{ type: String }],
  },
  { timestamps: true }
);
export default userSchema;
