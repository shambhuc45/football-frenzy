import { Router } from "express";
import {
  deleteUserController,
  forgotPassword,
  loginUserController,
  myProfile,
  readAllUserController,
  readSpecificUserController,
  registerUserController,
  resetPassword,
  updatePassword,
  updateProfile,
  updateSpecificUserController,
  verifyEmail,
} from "../controller/userController.js";
import authorized from "../middleware/authorized.js";
import isAuthenticated from "../middleware/isAuthenticated.js";

let userRouter = Router();
userRouter
  .route("/")
  .get(
    isAuthenticated,
    authorized(["admin", "superadmin"]),
    readAllUserController
  );
userRouter.route("/register").post(registerUserController);
userRouter.route("/verify").patch(verifyEmail);
userRouter.route("/login").post(loginUserController);
userRouter.route("/my-profile").get(isAuthenticated, myProfile);
userRouter.route("/update-profile").patch(isAuthenticated, updateProfile);
userRouter.route("/update-password").patch(isAuthenticated, updatePassword);
userRouter.route("/forgot-password").post(forgotPassword);
userRouter.route("/reset-password").patch(isAuthenticated, resetPassword);

userRouter
  .route("/:id")
  .get(
    isAuthenticated,
    authorized(["admin", "superadmin"]),
    readSpecificUserController
  )
  .patch(
    isAuthenticated,
    authorized(["admin", "superadmin"]),
    updateSpecificUserController
  )
  .delete(isAuthenticated, authorized("superadmin"), deleteUserController);

export default userRouter;
