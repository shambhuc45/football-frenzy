import { Router } from "express";
import {
  createProductController,
  deleteProductController,
  readAllProductsController,
  readSpecificProductController,
  updateSpecificProductController,
} from "../controller/productController.js";
import productValidation from "../validation/productValidation.js";
import { validation } from "../middleware/validation.js";

let productRouter = Router();
productRouter
  .route("/")
  .post(validation(productValidation), createProductController)
  .get(readAllProductsController);

productRouter
  .route("/:id")
  .get(readSpecificProductController)
  .patch(updateSpecificProductController)
  .delete(deleteProductController);
export default productRouter;
