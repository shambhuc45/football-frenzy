import express, { json } from "express";
import connectToMongoDb from "./src/connectToDB/connectToMongoDb.js";
import { port } from "./src/constant.js";
import productRouter from "./src/routes/productRouter.js";
import userRouter from "./src/routes/userRouter.js";
import cors from "cors";

const expressApp = express();

connectToMongoDb();
expressApp.use(json());
expressApp.use(cors());

expressApp.use("/users", userRouter);
expressApp.use("/products", productRouter);
expressApp.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}}`);
});
