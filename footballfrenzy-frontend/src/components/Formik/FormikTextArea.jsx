import { Field } from "formik";
import React from "react";

const FormikTextArea = ({ name, label, onChange, required, ...props }) => {
  return (
    <>
      <Field name={name}>
        {({ field, form, meta }) => {
          return (
            <div style={{ marginTop: "5px" }}>
              <label htmlFor={name}>
                {label}{" "}
                {required ? <span style={{ color: "red" }}>*</span> : null}:
              </label>
              <textarea
                {...field}
                {...props}
                id={name}
                value={meta.value}
                rows={5}
                onChange={onChange ? onChange : field.onChange}
              />
              {meta.error && meta.touched ? (
                <div style={{ color: "red" }}>{meta.error}</div>
              ) : null}
            </div>
          );
        }}
      </Field>
    </>
  );
};

export default FormikTextArea;
