import { Field } from "formik";
import React from "react";

const FormikRadio = ({
  name,
  label,
  onChange,
  required,
  options,
  ...props
}) => {
  return (
    <>
      <Field name={name}>
        {({ field, form, meta }) => {
          return (
            <div style={{ marginTop: "5px" }}>
              <label>
                {label}{" "}
                {required ? <span style={{ color: "red" }}>*</span> : null}:
              </label>

              {options.map((item, i) => {
                return (
                  <span key={i}>
                    <input
                      {...field}
                      {...props}
                      type="radio"
                      id={item.value}
                      value={item.value}
                      checked={meta.value === item.value}
                      onChange={onChange ? onChange : field.onChange}
                    />
                    <label htmlFor={item.value}>{item.label}</label>
                  </span>
                );
              })}

              {meta.error && meta.touched ? (
                <div style={{ color: "red" }}>{meta.error}</div>
              ) : null}
            </div>
          );
        }}
      </Field>
    </>
  );
};

export default FormikRadio;
