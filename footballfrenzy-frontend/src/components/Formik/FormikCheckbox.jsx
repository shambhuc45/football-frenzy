import { Field } from "formik";
import React from "react";

const FormikCheckbox = ({ name, label, onChange, required, ...props }) => {
  return (
    <>
      <Field name={name}>
        {({ field, form, meta }) => {
          return (
            <div style={{ marginTop: "5px" }}>
              <input
                {...field}
                {...props}
                type="checkbox"
                id={name}
                checked={meta.checked}
                onChange={onChange ? onChange : field.onChange}
              />
              <label htmlFor={name}>
                {label}?{" "}
                {required ? <span style={{ color: "red" }}>*</span> : null}
              </label>
              {meta.error && meta.touched ? (
                <div style={{ color: "red" }}>{meta.error}</div>
              ) : null}
            </div>
          );
        }}
      </Field>
    </>
  );
};

export default FormikCheckbox;
