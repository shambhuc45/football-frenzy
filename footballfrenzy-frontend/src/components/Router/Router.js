import {
  createBrowserRouter,
  createRoutesFromElements,
  Outlet,
  Route,
  RouterProvider,
} from "react-router-dom";
import Home from "../pages/Home";
import About from "../pages/about/About";
import Layout from "../Layout";
import Product from "../pages/Product";
import UserRegistration from "../pages/UserManagement/UserRegistration.jsx";

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Layout />}>
      <Route index={true} path="/" element={<Home />} />
      <Route
        path="users"
        element={
          <div>
            <Outlet></Outlet>
          </div>
        }
      >
        <Route
          path="register"
          element={<UserRegistration></UserRegistration>}
        ></Route>
        <Route path="login" element={<>Login Management</>}></Route>
      </Route>
      <Route path="product/:productId" element={<Product />} />
      <Route path="about" element={<About />} />

      <Route path="*" element={<div>404 page</div>} />
    </Route>
  )
);
export default function Router() {
  return <RouterProvider router={router} />;
}
