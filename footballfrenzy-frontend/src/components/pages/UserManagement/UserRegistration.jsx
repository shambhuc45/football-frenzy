import React from "react";
import { Form, Formik } from "formik";
import * as yup from "yup";
import FormikInput from "../../Formik/FormikInput";
import FormikRadio from "../../Formik/FormikRadio";
import { ToastContainer, toast } from "react-toastify";
import { hitApi } from "../../Refractor/hitApi.jsx";

const UserRegistration = () => {
  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Others", value: "others" },
  ];
  let initialValues = {
    fullName: "",
    email: "",
    password: "",
    phoneNumber: "",
    address: "",
    gender: "male",
  };
  let validationSchema = yup.object({
    fullName: yup
      .string()
      .required("Full Name is required.")
      .min(5, "Full Name must be of at least 3 Characters.")
      .max(20, "Full Name cannot exceed 30 characters"),
    email: yup
      .string()
      .required("Email is required.")
      .matches(
        /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
        "Please Enter a valid Email."
      ),
    password: yup
      .string()
      .required("Password is required.")
      .min(8, "Password must have at least 8 character long.")
      .matches(
        /^(?=.*\d)(?=.*[@$!%*?&#])(?=.*[A-Z]).+$/,
        "Password must contain at least 1 uppercase letter, one digit and 1 special character."
      ),
    phoneNumber: yup.string().required("Phone Number is required."),
    address: yup.string(),
    gender: yup.string().required("Please select your Gender."),
  });
  let handleSubmit = async (value, { resetForm }) => {
    console.log(value);
    let data = {
      ...value,
      role: "admin",
    };
    try {
      let response = await hitApi({
        url: "/",
        method: "POST",
        data: data,
      });
      toast.success(response.data.message);
      resetForm();
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };
  return (
    <>
      <ToastContainer></ToastContainer>
      <h2>Sign Up:</h2>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
        enableReinitialize={true}
      >
        {(formik) => {
          return (
            <div>
              <Form>
                <FormikInput
                  name="fullName"
                  label="Full Name"
                  inputType="text"
                  required={true}
                  placeholder="John Doe"
                  style={{ borderRadius: "5px" }}
                ></FormikInput>
                <FormikInput
                  name="email"
                  label="Email"
                  inputType="email"
                  required={true}
                  placeholder="abc12@gmail.com"
                  style={{ borderRadius: "5px" }}
                ></FormikInput>
                <FormikInput
                  name="password"
                  label="Password"
                  inputType="password"
                  required={true}
                  placeholder="********"
                  style={{ borderRadius: "5px" }}
                ></FormikInput>
                <FormikInput
                  name="phoneNumber"
                  label="Phone Number"
                  inputType="number"
                  required={true}
                  placeholder="##########"
                  style={{ borderRadius: "5px" }}
                ></FormikInput>
                <FormikInput
                  name="address"
                  label="Address"
                  inputType="text"
                  placeholder="Kathmandu"
                  style={{ borderRadius: "5px" }}
                ></FormikInput>

                <FormikRadio
                  name="gender"
                  label="Gender"
                  options={genders}
                  required={true}
                  checked="male"
                  style={{ borderRadius: "5px" }}
                ></FormikRadio>
                <button type="submit">Register</button>
              </Form>
            </div>
          );
        }}
      </Formik>
    </>
  );
};

export default UserRegistration;
