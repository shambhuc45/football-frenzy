import { Icon } from "@iconify-icon/react";
import React from "react";

export default function Services() {
  const servicesData = [
    {
      icon: "emojione-monotone:soccer-ball",
      title: "Sports Products",
      description:
        "Lorem Is Simply Dummy Text Of The Printing And Typesetting.",
    },
    {
      icon: "fluent:arrow-trending-lines-20-regular",
      title: "In-Demand Products",
      description:
        "Lorem Is Simply Dummy Text Of The Printing And Typesetting.",
    },
    {
      icon: "wpf:coins",
      title: "Best Prices",
      description:
        "Lorem Is Simply Dummy Text Of The Printing And Typesetting.",
    },
    {
      icon: "material-symbols-light:local-shipping-outline-rounded",
      title: "Free Delivery",
      description:
        "Lorem Is Simply Dummy Text Of The Printing And Typesetting.",
    },
  ];
  return (
    <>
      <div className="whyUs">
        <div className="whyUs__main">
          <h2 className="whyUs__main__heading">Why Choose Us?</h2>
          <p className="whyUs__main__description">
            Lorem Is Simply Dummy Text Of The Printing And Typesetting.
          </p>
          <button className="btn-primary whyUs__main--btn">Explore More</button>
        </div>
        <div className="whyUs__others">
          <div className="whyUs__others__cards">
            {servicesData.map((service, i) => (
              <div className="whyUs__others__cards__items" key={i}>
                <div className="whyUs__others__cards__items__icon">
                  <Icon
                    className="service-icon"
                    icon={service.icon}
                    width="5rem"
                  />
                </div>
                <div className="whyUs__others__cards__items__info">
                  <h4 className="whyUs__others__cards__items__info__title">
                    {service.title}
                  </h4>
                  <p className="whyUs__others__cards__items__info__description">
                    {service.description}
                  </p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  );
}
