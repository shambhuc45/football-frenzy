import React from "react";
import { Typewriter } from "react-simple-typewriter";
import barcaImage from "./../../../assets/heroImages/barca.jpg";
import madridImage from "./../../../assets/heroImages/madrid.jpg";
import bayernImage from "./../../../assets/heroImages/bayern.jpg";
import unitedImage from "./../../../assets/heroImages/United.jpg";
import liverpoolImage from "./../../../assets/heroImages/Liverpool.jpg";

export default function Herosection() {
  return (
    <>
      <div className="heroSection">
        <div className="heroSection__content">
          <div className="heroSection__content__text">
            <hr className=" hor-line-small" />
            <h1 className="heroSection__content__text--heading heading-primary">
              The Premium Product with pleasure.
            </h1>
            <div className="typewriter">
              <h4 style={{ color: "#1774d1" }}>
                SALE{" "}
                <span className="changing-text">
                  <Typewriter
                    words={["OFFER"]}
                    loop={false}
                    cursor
                    cursorStyle="|"
                    typeSpeed={100}
                    deleteSpeed={80}
                    delaySpeed={1000}
                  />
                </span>
              </h4>
            </div>
            <p>
              Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nam
              voluptatem temporibus sint, quisquam laboriosam accusantium.
            </p>
            <button className="btn-primary heroSection__content__text__btn">
              ORDER NOW
            </button>
          </div>
          <div className="heroSection__content__imageContainer">
            <div className="composition">
              <img
                src={barcaImage}
                alt="Barcelona"
                className="composition-image composition-image-pic1"
              />
              <img
                src={madridImage}
                alt="Madrid"
                className="composition-image composition-image-pic2"
              />
              <img
                src={bayernImage}
                alt="Bayern"
                className="composition-image composition-image-pic3"
              />
              <img
                src={unitedImage}
                alt="United"
                className="composition-image composition-image-pic4"
              />
              <img
                src={liverpoolImage}
                alt="Liverpool"
                className="composition-image composition-image-pic5"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
