import React, { useEffect, useMemo, useState } from "react";
import ProductCard from "./ProductCard";
import { Link } from "react-router-dom";
import axios from "axios";

export default function Categories() {
  const [activeBtn, setActiveBtn] = useState("featured");
  const [products, setProducts] = useState([]);
  const [filteredProducts, setFilteredProducts] = useState([]);
  const buttonData = useMemo(
    () => [
      {
        id: "featured",
        btnText: "Featured",
      },
      {
        id: "topSelling",
        btnText: "Top Sellings",
      },
      {
        id: "allJersey",
        btnText: "All Jerseys",
      },
      {
        id: "graphicTees",
        btnText: "Graphic T-shirts",
      },
      {
        id: "phoneCase",
        btnText: "Phone Case",
      },
      {
        id: "ornaments",
        btnText: "Ornaments",
      },
    ],
    []
  );
  const fetchProducts = async () => {
    try {
      const response = await axios.get("http://localhost:8000/products");
      setProducts(response.data.result);
    } catch (error) {
      console.error("Error fetching products:", error);
    }
  };

  useEffect(() => {
    fetchProducts();
  }, []);
  useEffect(() => {
    const filtered = products.filter((product) =>
      product.categories.includes(activeBtn)
    );
    setFilteredProducts(filtered);
  }, [activeBtn, products]);
  return (
    <>
      <section className="categories row">
        <hr className="hor-line-small" />
        <div className="categories__container">
          <div className="categories__container--btngroup margin-bottom-small">
            {buttonData.map((category) => (
              <button
                key={category.id}
                className={`btn-category ${
                  activeBtn === category.id ? "active" : ""
                }`}
                onClick={() => setActiveBtn(category.id)}
              >
                {category.btnText}
              </button>
            ))}
          </div>
          <div className="product">
            {filteredProducts ? (
              <>
                {filteredProducts.map((product) => (
                  <Link to={`/product/${product._id}`}>
                    <ProductCard
                      key={product._id}
                      src={product.imageUrls[0]}
                      title={product.title}
                      actualPrice={product.actualPrice}
                      sellingPrice={product.sellingPrice}
                      rating={product.rating}
                      // onClick={handleProductClick}
                      productDetails={product}
                    />
                  </Link>
                ))}
              </>
            ) : (
              <>
                <p style={{ color: "red" }}>No products Found:</p>
              </>
            )}
          </div>
        </div>
      </section>
    </>
  );
}
