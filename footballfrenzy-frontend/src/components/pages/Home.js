import React from "react";
import Herosection from "./homeFragments/Herosection";
import Services from "./homeFragments/Services";
import RequestForm from "./homeFragments/RequestForm";
import Categories from "./homeFragments/Categories";
export default function Home() {
  return (
    <>
      <Herosection />
      <Services />
      <Categories />
      <RequestForm />
    </>
  );
}
