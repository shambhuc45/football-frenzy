import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import ProductDetails from "./product/ProductDetails";
import ImageCarousel from "./product/ImageCarousel";

export default function Product() {
  const [product, setProduct] = useState({});
  const getParams = useParams();
  const productId = getParams.productId;
  const [description, setDescription] = useState([]);
  const [imageUrls, setImageUrls] = useState([]);

  const fetchProduct = async () => {
    try {
      const response = await axios.get(
        `http://localhost:8000/products/${productId}`
      );
      setProduct(response.data.result);
      setDescription(response.data.result.description);
      setImageUrls(response.data.result.imageUrls);
    } catch (error) {
      console.error("Error fetching products:", error);
    }
  };

  useEffect(() => {
    fetchProduct();
  }, []);

  // return <>{<SingleDetailed productId={productId} />}</>;
  return (
    <>
      <section className="product-details row">
        <div className="product-contents">
          <ImageCarousel imageUrls={imageUrls} />
          <ProductDetails
            title={product.title}
            description={description}
            sellingPrice={product.sellingPrice}
            actualPrice={product.actualPrice}
            discount={product.discount}
          />
        </div>
      </section>
    </>
  );
}
