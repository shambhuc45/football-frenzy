import React, { useEffect, useState } from "react";

export default function ImageCarousel({ imageUrls }) {
  const [activeImageSlide, setActiveImageSlide] = useState(0);
  const [backgroundImage, setBackgroundImage] = useState(null);

  const handleImageClick = (index) => {
    setActiveImageSlide(index);
    setBackgroundImage(imageUrls[index]);
  };

  useEffect(() => {
    setBackgroundImage(imageUrls[0]);
  }, [imageUrls]);

  return (
    <>
      <div
        className="image-slider"
        style={{ backgroundImage: `url(${backgroundImage})` }}
      >
        <div className="product-images">
          {imageUrls.map((imageUrl, index) => (
            <img
              key={index}
              src={imageUrl}
              className={index === activeImageSlide ? "active" : ""}
              alt="Product pic"
              onClick={() => handleImageClick(index)}
            />
          ))}
        </div>
      </div>
    </>
  );
}
