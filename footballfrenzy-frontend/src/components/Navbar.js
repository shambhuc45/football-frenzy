import React from "react";
import { Icon } from "@iconify/react";
import { Link } from "react-router-dom";
// import logo from "../assets/logo.png";

export default function Navbar() {
  return (
    <>
      <header className="navigation">
        <div className="nav">
          <div className="nav__logo">
            <Link to="/" className="nav__logo__img">
              <img src="/logo.png" alt="Frenzy Logo" />
            </Link>
          </div>
          <div className="nav__items">
            <div className="nav__items__search">
              <input
                id="search-box"
                type="text"
                className="nav__items__search__box"
                placeholder="Search Item Here"
              ></input>
              <button className="nav__items__search__btn">Search</button>
            </div>
            <div className="nav__items__icons">
              <Link to="/">
                <Icon className="icons" icon="bytesize:cart" width="3.5rem" />
              </Link>
              <Link to="/users/register">
                <Icon
                  className="icons"
                  icon="iconoir:profile-circle"
                  width="3.5rem"
                />
              </Link>
            </div>
          </div>
        </div>
        <nav className="navbar">
          <hr className="hor-line" />
          <ul className="navbar__list">
            <li className="navbar__list__item">
              <Link to="/" className="item-hyperlink">
                Home
              </Link>
            </li>
            <li className="navbar__list__item">
              <Link to="/club" className="item-hyperlink">
                Club
              </Link>
            </li>
            <li className="navbar__list__item">
              <Link to="/national" className="item-hyperlink">
                National
              </Link>
            </li>
            <li className="navbar__list__item">
              <Link to="/cases" className="item-hyperlink">
                Phone Cases
              </Link>
            </li>
            <li className="navbar__list__item">
              <Link to="/ornament" className="item-hyperlink">
                Ornaments
              </Link>
            </li>
          </ul>
        </nav>
      </header>
    </>
  );
}
