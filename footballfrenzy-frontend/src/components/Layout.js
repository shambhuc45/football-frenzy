import Navbar from "./Navbar";
import Footer from "./Footer";
import { Outlet } from "react-router-dom";
import ScrollToTopOnRouteChange from "./ScrollToTopOnRouteChange";

export default function Layout() {
  return (
    <>
      <Navbar />
      <Outlet />
      <Footer />
      <ScrollToTopOnRouteChange />
    </>
  );
}
