import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./index.css";
import Router from "./components/Router/Router";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Router>
      <App />
    </Router>
  </React.StrictMode>
);
